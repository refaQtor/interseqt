#Copyright (c) 2014-2016, shannon.mackey@refaqtory.com

QT += core
QT -= gui

CONFIG += c++11

TARGET = interseqt-cli
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../interseqt/release/ -llibInterseqt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../interseqt/debug/ -llibInterseqt
else:unix: LIBS += -L$$OUT_PWD/../interseqt/ -llibInterseqt

INCLUDEPATH += $$PWD/../interseqt
DEPENDPATH += $$PWD/../interseqt

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../interseqt/release/liblibInterseqt.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../interseqt/debug/liblibInterseqt.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../interseqt/release/libInterseqt.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../interseqt/debug/libInterseqt.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../interseqt/liblibInterseqt.a
