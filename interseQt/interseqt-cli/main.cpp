/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2014-2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include <QCoreApplication>

int main(int argc, char* argv[])
{
    QCoreApplication::setOrganizationName("refaqtory");
    QCoreApplication::setOrganizationDomain("refaqtory.com");
    QCoreApplication::setApplicationName("interseqt");
    QCoreApplication::setApplicationVersion("0.1");

    QCoreApplication a(argc, argv);

    return a.exec();
}

//TODO: veqtor abstract class interface, read/write file
//TODO: System/Machine/Job structs
//TODO: veqtor subclass with getSystemState, putMachineState, putJobState
//TODO: interseqt startup clone
//TODO: interseqt insert self, branch (per machine or job?)
//TODO: interseqt negotiate job
//TODO: interseqt do job
//TODO: interseqt put results
//TODO:
//TODO: cli insert job
//TODO: gui view state
