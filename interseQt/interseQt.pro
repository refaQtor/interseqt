#Copyright (c) 2014-2016, shannon.mackey@refaqtory.com
TEMPLATE = subdirs

SUBDIRS += \
    interseqt \
    InterseqtTest \
    interseqt-cli \
    interseqt-gui
