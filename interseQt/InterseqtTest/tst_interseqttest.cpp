/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2014-2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include <QString>
#include <QtTest>

#include "interseqt.h"

class InterseqtTest : public QObject {
    Q_OBJECT

    Interseqt* chuck;

public:
    InterseqtTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void initRepo();
    void initLocalFossil();
    void initLocalGit();
    void initLocalMonotone();
};

InterseqtTest::InterseqtTest()
{
    QCoreApplication::setOrganizationName("refaqtory");
    QCoreApplication::setOrganizationDomain("refaqtory.com");
    QCoreApplication::setApplicationName("interseqt");
    QCoreApplication::setApplicationVersion("0.1");
}

void InterseqtTest::initTestCase()
{
}

void InterseqtTest::cleanupTestCase()
{
}

void InterseqtTest::initRepo()
{
    QVERIFY2(true, "Failure");
}

void InterseqtTest::initLocalFossil()
{
}

QTEST_APPLESS_MAIN(InterseqtTest)

#include "tst_interseqttest.moc"
