/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2014-2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include "gitinterface.h"

#include <QDebug>
#include <QDir>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QSet>
#include <QProcess>
#include <QStandardPaths>

GitInterface::GitInterface(QString working_path, QObject *parent) :
    QObject(parent),
    _working_path(QDir::toNativeSeparators(working_path)),
    git_run(new QProcess()),
    _output(""),
    _contents("")
{
    Q_ASSERT(!_working_path.isEmpty());
    QString repo_path(QDir::toNativeSeparators( _working_path + "/.git"));
    if (!QFile::exists(repo_path)) {
        QDir work;
        work.mkpath(_working_path.toLocal8Bit());
        Q_ASSERT(QFile::exists(work.canonicalPath()));
        gitInit();
    }
}

GitInterface::~GitInterface()
{
}

void GitInterface::gitInit()
{
    //TODO: set local git user here
    qDebug()<< gitOutput(QStringList() << "init");
    QString ignore_file(QDir::toNativeSeparators(QDir::cleanPath( _working_path + "/.gitignore")));
    if (!QFile::copy(":/resources/gitignore.txt", ignore_file)) {
        qDebug() << "initialization failed : " << ignore_file;
    } else {
        Q_ASSERT(QFile::exists(ignore_file));
        gitCommit("git init");
    }
}

void GitInterface::gitCommit(QString note)
{
    qDebug()<< gitOutput(QStringList() << "add" << ".");
    qDebug()<< gitOutput(QStringList() << "commit" << "-m" << note.replace(":","-").replace("\"","-"));
}

QString GitInterface::gitOutput(QStringList args)
{
    qDebug() << _working_path << "  - Capacitor::gitOutput: git " << args;
    if (!QDir::setCurrent(_working_path))
        qDebug() << "ERROR: QDir::setCurrent: " << _working_path;
    git_run = new QProcess(qobject_cast<QObject *>(this));
    git_run->setReadChannel(QProcess::StandardOutput);
    git_run->setWorkingDirectory(_working_path);
    git_run->start("git", args);
    bool ok = git_run->waitForStarted();
    Q_ASSERT(ok);
    ok = git_run->waitForReadyRead();
   // Q_ASSERT(ok);
    ok = git_run->waitForFinished();
    QString output(git_run->readAll());
    git_run->deleteLater();
//    qDebug() << output;
    return output;
}

QByteArray GitInterface::gitFileContents(quint32 index, QString filename)
{
    QFileInfo fi(filename);
    QString data("HEAD~" + QString::number(index) + ":" + fi.fileName());
    _contents.clear();
    _contents = gitOutput(QStringList() << "show" << data).toLocal8Bit();
//    qDebug() << _contents;
    return (!_contents.isEmpty()) ? _contents : "Selected file was not being followed at the time of selected capture.";
}

QString GitInterface::gitStat()
{
    return gitOutput(QStringList() << "log" << "--stat");
}



