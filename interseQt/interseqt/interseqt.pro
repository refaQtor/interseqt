#Copyright (c) 2014-2016, shannon.mackey@refaqtory.com

QT       -= gui

TARGET = libInterseqt
TEMPLATE = lib
CONFIG += staticlib

SOURCES += interseqt.cpp \
    mapfile.cpp \
    gitinterface.cpp

HEADERS += interseqt.h \
    mapfile.h \
    processconnection.h \
    gitinterface.h
unix {
    target.path = /usr/libR
    INSTALLS += target
}
